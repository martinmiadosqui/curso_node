import express from 'express';
import * as productos from './productos';

const app = express();
app.use(express.json());

app.get('/',(req,res)=>{
    res.send('Bienvenidos a Express');
});

app.get('/productos',(req,res)=>{
    res.send(productos.getStock());
});

app.post('/productos',(req,res)=> {
    const body=req.body;
    const idProducto= productos.getStock().length+1;
    productos.storeProductos(body,idProducto);
    res.send("Se guardó el producto exitosamente");
})

app.put('/productos/:id',(req,res)=>{
    const body=req.body;
    const idProducto=Number (req.params.id);
    productos.editarProductos(body,idProducto);
    res.send("Cambio exitoso");
}); 

app.delete('/productos/:id',(req,res)=> {
    const idProducto=Number (req.params.id);
    productos.deleteProductos(idProducto);
    res.send("Se eliminó el producto exitosamente");
    
});

app.listen(3000,()=>{
    console.info('Servidor escuchado en http://localhost:3000');
});

