// productos.ts
// Creando una lista de productos
interface Producto { 
    id:number,
    nombre: string; 
    precio: number;
    stock: number; 
  } 
  //
  const Producto1: Producto = {
    id:1, 
    nombre: "Yogurt", 
    precio: 100,
    stock: 13
  }; 
  
  const Producto2: Producto = { 
   id:2, 
   nombre: "Paquete de Galletas", 
   precio: 89.9,
   stock: 40
 };

 const Producto3: Producto = { 
  id:3,
  nombre: "Lata Coca-Cola", 
  precio: 80.50,
  stock: 0
 };

 const Producto4: Producto = {
  id:4,
  nombre: "Alfajor",
  precio: 40,
  stock: 12
  
 }
 
 
 let productos:Array<Producto> = [
   Producto1,
   Producto2,
   Producto3,
   Producto4
 ]
 console.log(productos);
  
export function getStock(){
  return productos;
}

export function storeProductos(body:any,id:any){
  productos.push({
    id: id,
    nombre: body.nombre,
    precio: body.precio,
    stock: body.stock
   });
   console.log(productos);

   
}

export function editarProductos(body:any,indice:number){
  
  productos[indice].precio=body.precio;
  productos[indice].stock=body.stock;

  console.log(productos);
      
} 

export function deleteProductos(indice:number){
  productos.splice(indice);
  console.log(productos);
}
