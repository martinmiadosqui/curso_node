"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteProductos = exports.editarProductos = exports.storeProductos = exports.getStock = void 0;
//
const Producto1 = {
    id: 1,
    nombre: "Yogurt",
    precio: 100,
    stock: 13
};
const Producto2 = {
    id: 2,
    nombre: "Paquete de Galletas",
    precio: 89.9,
    stock: 40
};
const Producto3 = {
    id: 3,
    nombre: "Lata Coca-Cola",
    precio: 80.50,
    stock: 0
};
const Producto4 = {
    id: 4,
    nombre: "Alfajor",
    precio: 40,
    stock: 12
};
let productos = [
    Producto1,
    Producto2,
    Producto3,
    Producto4
];
console.log(productos);
function getStock() {
    return productos;
}
exports.getStock = getStock;
function storeProductos(body, id) {
    productos.push({
        id: id,
        nombre: body.nombre,
        precio: body.precio,
        stock: body.stock
    });
    console.log(productos);
}
exports.storeProductos = storeProductos;
function editarProductos(body, indice) {
    productos[indice].precio = body.precio;
    productos[indice].stock = body.stock;
    console.log(productos);
}
exports.editarProductos = editarProductos;
function deleteProductos(indice) {
    productos.splice(indice);
    console.log(productos);
}
exports.deleteProductos = deleteProductos;
