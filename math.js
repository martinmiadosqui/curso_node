// funcion con nombre o nombrada
function suma(x, y) {
    return x + y;
}
function resta(x, y) {
    return x - y;
}
function multiplicacion(x, y) {
    return x * y;
}
function division(x, y) {
    if (y == 0) {
        console.log("No se puede dividir por cero");
        return 0;
    }
    else {
        return x / y;
    }
}
console.log(suma(9, 1));
console.log(resta(25, 5));
console.log(multiplicacion(9, 9));
console.log(division(81, 0));
console.log(division(81, 9));
// funcion flecha
var sumaFlecha = function (x, y) {
    return x + y;
};
var restaFlecha = function (x, y) {
    return x - y;
};
var multiFlecha = function (x, y) {
    return x * y;
};
var divFlecha = function (x, y) {
    if (y == 0) {
        console.log("No se puede dividir por cero");
        return 0;
    }
    else {
        return x / y;
    }
};
console.log(sumaFlecha(1, 2));
console.log(restaFlecha(1, 4));
console.log(multiFlecha(25, 5));
console.log(divFlecha(1, 0));
console.log(divFlecha(18, 6));
// funcion con parametros opcionales
var funcionOpcionalSuma = function (x, y) {
    if (!y)
        return x;
    return x + y;
};
var funcionOpcionalResta = function (x, y) {
    if (!y)
        return x;
    return x - y;
};
console.log(funcionOpcionalSuma(1));
console.log(funcionOpcionalSuma(1, 2));
console.log(funcionOpcionalResta(22, 2));
// Otra opcion es agregarle un valor por defecto
var funcionOpcionalSuma2 = function (x, y) {
    if (y === void 0) { y = 0; }
    return x + y;
};
console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1, 2));
