"use strict";
exports.__esModule = true;
var express_1 = require("express");
var productos = require("./productos");
var app = (0, express_1["default"])();
app.get('/', function (req, res) {
    res.send('Hello World');
});
app.get('/productos', function (req, res) {
    res.send(productos.getStock());
});
app.listen(3000, function () {
    console.info('Servidor en http://localhost:3000');
});
